<?php

class Robot {
    
    const ROBOT_FILE = '/robots.txt';
    const REPORT_PATH = __DIR__ . '/../config/report.php';
    /**
     *
     * @var type 
     */
    public $report;
    /**
     *
     * @var type 
     */
    public $file;

    public function __construct($siteUrl) {

        $this->file = $this->getFile();
        $this->report = $this->getReport();
    }

    public function getFile($siteUrl) {
        if (file_exists($siteUrl . self::ROBOT_FILE) and is_readable($siteUrl . self::ROBOT_FILE)) {
            return require $siteUrl . self::ROBOT_FILE;
        } else {
            return false;
        }
    }

    public function getReport() {
        if(file_exists(self::REPORT_PATH) and is_readable(self::REPORT_PATH)){
            return require self::REPORT_PATH;
        }else{
            return false;
        }
    }

}
